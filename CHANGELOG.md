# Changelog

This project is a template for APIs build with Koa. Has error handler, logger
and resolver middlewares.

## initial release 1.0.0

Launch with a base router that has a `GET '/'` that returns an `api.html` to
render on the browser to see if the api it's working.

Implements a simple JWT.

- Created and configured all the base files
  - index.js
  - src/
    - server.js
    - api.html
    - middlewares/
      - error_handler.js
      - logger.js
      - resolver.js
    - routes/
      - index.js
    - usecase/
    - models/
    - lib/
      - bcrypt.js
      - jwt.js
- defined the base `example.env`
- created api.html as entry point

## vanilla server 1.0.1

Removed knex, postgresql, objection dependencies and scripts to have a vanilla
koa app that adapts to any db.

Updated README.md with project structure.

Adds LICENSE file.

## PostgreSQL Flavor 1.0.1 P

Configured the app to use a PostgreSQL database using Knex and Objection.
