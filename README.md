# Template KOA Server

![NodeJS][node-badge]
![KoaJS][koa-badge]
![JWT][jwt-badge]
![BCrypt][bcrypt-badge]
![Husky][husky-badge]
![StandardJS][standard-badge]

This repository it's a template project that runs a simple api created with koa
that implements a custom error handler, logger and resolver.

The project was designed to reduce the boilerplate when creating APIs with koa.
It implements [Standard JS](https://standardjs.com/) and enforce it using
[Husky git hooks](https://github.com/typicode/husky). It's pre configured to
create a simple server listening to port `8080`

## Running

First install all dependencies with `npm i`

Then run the project with `npm run dev`

## Project Structure

### Files

`index.js` Entry point, it loads the server, env variable and starts the project
in the port __8080__.

`src/server.js` Koa server configurations, creates a Koa App and add all the
middlewares and routers. Has a __baseRouter__ that has one route __GET '/'__ to
show a simple __api.html__ page when visited in the browser.

`src/routes/index.js` Collects all the custom routes to export them to be loaded
in server.

`src/constants/` Has all the constant files that the project needs.

`src/lib/` Stores all the configuration files for the libs.

`src/lib/jwt.js` Configures the JWT implementation.

`src/lib/bcrypt.js` Configure the bcrypt library.

`src/middlewares/` Stores all the custom middlewares.

`src/middlewares/error_handler.js` Middleware that adds a global __try catch__
statement to handle errors.

`src/middlewares/logger.js` Middleware that adds a request/response logger.

`src/middlewares/resolver.js` Middleware that adds a resolver to generalize the
response from the routes.

`src/models` Directory for models if needed. Has a __.gitkeep__ file to version
the empty dir.

`src/usecase` Directory for the models usecases. Has a __.gitkeep__ file to version
the empty dir.

`example.env` Example of how the __.env__ file should be.

## Making Commits

The project implements a simple __pre-commit git hook__ implemented with husky
that runs standard js to ensure the files respect the code style.

[node-badge]: https://badgen.net/badge/NodeJS/LTS/339933
[koa-badge]: https://badgen.net/badge/KoaJS/2.13.0/000000
[jwt-badge]: https://badgen.net/badge/JWT/8.5.1/000000
[bcrypt-badge]: https://badgen.net/badge/BCrypt/5.0.0/000000
[husky-badge]: https://badgen.net/badge/Husky/4.2.5/grey
[standard-badge]: https://badgen.net/badge/StandardJS/14.3.4/F7DF1E
