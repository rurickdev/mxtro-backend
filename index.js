require('dotenv').config()
const colors = require('colors/safe')

const db = require('./src/lib/db')
const server = require('./src/server')
const separators = require('./src/constants/separators')

function startServer () {
  return new Promise((resolve, reject) => {
    server.listen(8080, () => {
      resolve()
    })
  })
}

async function main () {
  console.log(separators.top)
  console.log(colors.yellow('│ STARTING...                                  │'))
  console.log(separators.middle)

  await db.raw('SELECT \'ping\' as ping;')
  console.log(colors.blue('│ PostgreSQL DB connected                      │'))
  console.log(separators.middle)

  await startServer()
  console.log(colors.blue('│ Server Listening in'), colors.green('http://localhost:8080    │'))
  console.log(separators.middle)
}

main()
  .then(() => {
    console.log('│ API READY                                    │')
    console.log(separators.bottom)
    console.log(separators.double)
  })
  .catch(error => {
    console.error(colors.red(`│ ERROR: ${error}`))
    console.error(separators.bottom)
  })
