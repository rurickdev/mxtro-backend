require('dotenv').config()

const {
  POSTGRESQL_DB_HOST,
  POSTGRESQL_DB_USER,
  POSTGRESQL_DB_PASSWORD,
  POSTGRESQL_DB_DATABASE_NAME,
  LOCAL_POSTGRESQL_DB_USER,
  LOCAL_POSTGRESQL_DB_PASSWORD,
  LOCAL_POSTGRESQL_DB_DATABASE_NAME
} = process.env

module.exports = {
  development: {
    client: 'pg',
    connection: {
      host: 'localhost',
      database: LOCAL_POSTGRESQL_DB_DATABASE_NAME,
      user: LOCAL_POSTGRESQL_DB_USER,
      password: LOCAL_POSTGRESQL_DB_PASSWORD
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
  production: {
    client: 'pg',
    connection: {
      host: POSTGRESQL_DB_HOST,
      database: POSTGRESQL_DB_DATABASE_NAME,
      user: POSTGRESQL_DB_USER,
      password: POSTGRESQL_DB_PASSWORD
    },
    pool: {
      min: 1,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

}
