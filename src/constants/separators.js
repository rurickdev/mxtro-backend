const colors = require('colors/safe')

module.exports = {
  top: colors.cyan('┌──────────────────────────────────────────────┐'),
  middle: colors.cyan('├──────────────────────────────────────────────┤'),
  bottom: colors.cyan('└──────────────────────────────────────────────┘'),
  double: colors.cyan('════════════════════════════════════════════════')
}
