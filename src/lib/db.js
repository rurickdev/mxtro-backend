const colors = require('colors/safe')
const Knex = require('knex')
const { knexSnakeCaseMappers } = require('objection')

const {
  POSTGRESQL_DB_HOST,
  POSTGRESQL_DB_USER,
  POSTGRESQL_DB_PASSWORD,
  POSTGRESQL_DB_DATABASE_NAME
} = process.env

const knex = Knex({
  client: 'pg',
  connection: {
    host: POSTGRESQL_DB_HOST,
    user: POSTGRESQL_DB_USER,
    password: POSTGRESQL_DB_PASSWORD,
    database: POSTGRESQL_DB_DATABASE_NAME
  },
  pool: {
    min: 1,
    max: 5,
    afterCreate: (connection, done) => {
      connection.query("SELECT 'ping' as ping;", (error) => {
        if (!error) console.info(colors.blue('Knex pool OK'))
        if (error) console.error(colors.red('Knex pool FAILED'))
        done(error, connection)
      })
    }
  },
  ...knexSnakeCaseMappers()
})

module.exports = knex
