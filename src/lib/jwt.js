const jwt = require('jsonwebtoken')

const {
  JWT_SECRET,
  JWT_EXPIRATION
} = process.env

function sign (payload, secret = JWT_SECRET) {
  // Converting [jwt.sing] method to a promise
  return new Promise((resolve, reject) => {
    jwt.sign(
      payload,
      secret,
      { expiresIn: JWT_EXPIRATION },
      (error, token) => {
        if (error) return reject(error)
        resolve(token)
      })
  })
}

function verify (token, secret = JWT_SECRET) {
  // Converting [jwt.verify] method to a promise
  return new Promise((resolve, reject) => {
    jwt.verify(
      token,
      secret,
      (error, decoded) => {
        if (error) return reject(error)
        resolve(decoded)
      })
  })
}

module.exports = {
  sign,
  verify
}
