/**
 * Handle all the error that the app throws and send the
 * corresponding response to the clients.
 */
async function errorHandler (context, next) {
  try {
    await next()
  } catch (error) {
    context.status = error.status || 500
    context.body = {
      success: false,
      message: error.message
    }
    context.app.emit('error', error, context)
  }
}

module.exports = errorHandler
