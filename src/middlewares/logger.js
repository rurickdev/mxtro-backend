const colors = require('colors/safe')

const separators = require('../constants/separators')

function formatObject (object) {
  return JSON.stringify(object, null, 2)
    .replace(/\n/g, '\n│ ')
}

function cleanBody (body) {
  const bodyToClean = typeof body === 'string' ? JSON.parse(body) : body
  let cleanedBody = { ...bodyToClean }

  if ('password' in cleanedBody) {
    cleanedBody = {
      ...cleanedBody,
      password: '<PASSWORD>'
    }
  }

  if ('token' in cleanedBody) {
    cleanedBody = {
      ...cleanedBody,
      token: '<TOKEN>'
    }
  }

  return cleanedBody
}

function formatInMessage (context) {
  const body = `│ REQUEST BODY:${formatObject(cleanBody(context.request.body))}`
  const method = `│ METHOD: ${context.method}`
  const query = `│ QUERY: ${formatObject(context.query)}`
  const path = `│ PATH: ${context.url}`
  const time = `│ TIME: ${new Date().toISOString()}`

  return colors.cyan(`${time}\n${method}\n${path}\n${query}\n${body}`)
}

function formatOutMessage (context, duration, hasError = false) {
  const body = `│ RESPONSE BODY: ${formatObject(cleanBody(context.response.body))}`
  const formattedDuration = `│ DURATION: ${duration} ms`
  const responseStatus = `│ STATUS: ${context.status}`
  const title = hasError ? 'FAIL' : 'SUCCESS'
  const color = hasError ? colors.red : colors.green

  return color(`│ ${title}\n${responseStatus}\n${formattedDuration}\n${body}`)
}

/**
 * Logs all request and response of the app
 */
async function logger (context, next) {
  const startDate = Date.now()
  try {
    console.log(separators.top)
    console.log(formatInMessage(context))
    console.log(separators.middle)
    await next()
    const duration = Date.now() - startDate
    console.log(formatOutMessage(context, duration))
    console.log(separators.bottom)
    console.log(separators.double)
  } catch (error) {
    const duration = Date.now() - startDate
    console.error(formatOutMessage(context, duration, true))
    console.error(separators.bottom)
    console.error(separators.double)
    throw error
  }
}

module.exports = logger
