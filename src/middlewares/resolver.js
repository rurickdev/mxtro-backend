/**
 * Adds a [resolve] function to [context] object
 */
async function resolve (context, next) {
  /**
   * Format the response body.
   *
   * @param {object} data data fot the body.
   *
   * @param {number} [status = 200] status number of the response.
   * @param {string} [message = DONE!] message of the response.
   * @param {object} [payload = {}] payload of the response.
   * @param {object} [extra = {}] extra data of the response.
   */
  context.resolve = ({ status = 200, message = 'DONE!', payload = {}, extra = {} } = {}) => {
    context.body = {
      success: true,
      message,
      payload,
      ...extra
    }
  }

  return next()
}

module.exports = resolve
