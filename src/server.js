const fs = require('fs')
const Koa = require('koa')
const koaBody = require('koa-body')
const koaCors = require('kcors')
const Router = require('koa-router')

// Custom middlewares
const errorHandler = require('./middlewares/error_handler')
const logger = require('./middlewares/logger')
const resolver = require('./middlewares/resolver')

const app = new Koa()
const baseRouter = new Router()

const environment = process.env.NODE_ENV || 'development'

// Returns an index page when visited from browser
baseRouter.get('/', async (context, next) => {
  context.type = 'html'
  const indexPath = `${__dirname}/api.html`
  context.body = fs.createReadStream(indexPath)
  return next()
})

// only use helmet in production
if (environment === 'production') {
  const koaHelmet = require('koa-helmet')
  app.use(koaHelmet())
}

// Load all routers
const routers = {
  baseRouter,
  ...require('./routes')
}

// Use middlewares
app.use(koaCors())
app.use(koaBody({ multipart: true, formidable: { maxFileSize: 10000000 } }))
app.use(errorHandler)
app.use(resolver)
app.use(logger)

// Configure app to use all routers
Object.values(routers).map(
  router => app.use(router.routes()).use(router.allowedMethods())
)

module.exports = app
